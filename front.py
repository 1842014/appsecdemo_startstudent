from flask import Flask, request, jsonify, make_response, render_template
import requests
import json
import jinja2

app = Flask('FrontEnd')

BACK_END_IP = '127.0.0.1'
BACK_END_PORT = '5555'

def build_home_page(messages):
	ip = request.remote_addr

	response = requests.get('http://' + BACK_END_IP + ':' + BACK_END_PORT+ '/', json={'ip':ip})
	data = None
	if response.status_code == 200:
		data = json.loads(response.content.decode('utf-8'))

	else:
		data = {'message':'ERROR'}

	content_basic = str(data)

	content_action = str(messages)


	return '''<html>
	<header></header>
	</body>
		<li><a href="/">HOME</a></li>
	<br>
	<br>
	BASIC DATA:
		<ul>
			<li>''' + content_basic + '''</li>
		</ul>
	<br>
	<br>
	ACTION DATA:
		<ul>
			<li>''' + content_action + '''</li>
		</ul>

	<br>
	<br>
	LOGIN:<br>
	<form action="/login" method="POST">
		<label>Username : </label>
		<input type="text" placeholder="Enter Username" name="username" required>
		<label>Password : </label>
		<input type="password" placeholder="Enter Password" name="password" required>
	<button type="submit">Login</button>
	</form>
	<br>
	<br>
	LOGOUT:<br>
	<form action="/logout" method="POST">
	        <button type="submit">Logout</button>
       </form>
	<br>
	<br>
	CREATE USER:<br>
       <form action="/createuser" method="POST">
               <label>username : </label>
               <input type="text" placeholder="provide the new username" name="name" required>
               <label>password : </label>
               <input type="text" placeholder="provide your password" name="password" required>
	        <button type="submit">Submit</button>
	</form>
	<br>
	<br>
	VIEW USERS: <br>
	<ul>
		<li><a href="/viewusers">View All users</a></li>
	</ul>

	CREATE TASK:<br>
	<form action="/createtask" method="POST">
		<label>Task : </label>   
		<input type="text" placeholder="Describe your task" name="task" required>  
	<button type="submit">Submit</button>  
	</form>
	<br>
	<br>
	VIEW TASKS:<br>
	<ul>
		<li><a href="/viewtasks">View All Tasks</a></li>
		<li> <form action="/viewtask" method="POST">
	                <label>TaskID : </label>
	               	<input type="text" placeholder="Id of your task" name="taskID" required>
		        <button type="submit">Submit</button>
	        	</form>
		</li>
	</ul>
	</body>
	</html>'''


def build_response(response):
	if response.status_code == 200:
		obj = json.loads(response.content.decode('utf-8'))
		resp = make_response(build_home_page(obj))
		return resp
	else:
		msg = response.content.decode('utf-8')
		resp = make_response(build_home_page({'message':msg}))
		return resp


@app.route('/')
def hello():
	return build_home_page({})



@app.route('/createtask', methods=['POST'])
def createtask():
	data = {'text':request.form['task'], 'ip':request.remote_addr}
	print(data, flush=True)
	resp = requests.post('http://' + BACK_END_IP + ':' + BACK_END_PORT + '/task', json=data)
	return build_response(resp)

@app.route('/createuser', methods=['POST'])
def createuser():
	data = {'name': request.form['name'], 'password': request.form['password'], 'ip': request.remote_addr}
	print(data, flush=True)
	resp = requests.post('http://' + BACK_END_IP + ':' + BACK_END_PORT + '/user', json=data)
	return build_response(resp)

@app.route('/viewusers', methods=['GET'])
def viewUsers():
	data = {'ip':request.remote_addr}
	response = requests.get('http://' + BACK_END_IP + ':' + BACK_END_PORT + '/user', json=data)
	return build_response(response)


@app.route('/viewtasks', methods=['GET'])
def viewTasks():
	data = {'ip':request.remote_addr}
	response = requests.get('http://' + BACK_END_IP + ':' + BACK_END_PORT + '/task', json=data)
	return build_response(response)


@app.route('/viewtask', methods=['POST'])
def viewTask():
	task_id = request.form['taskID']

	data = {'ip':request.remote_addr}
	response = requests.get('http://' + BACK_END_IP + ':' + BACK_END_PORT + '/task/' + task_id, json=data)
	return build_response(response)

@app.route('/login', methods=['POST'])
def login():
	ip = request.remote_addr
	data = {'ip':ip}
	response = requests.post('http://' + BACK_END_IP + ':' + BACK_END_PORT + '/login', auth=(request.form['username'], request.form['password']), json=data)
	resp = build_response(response)

	if response.status_code == 200:
		obj = json.loads(response.content.decode('utf-8'))
	return resp

if __name__ == '__main__':
        app.run(debug=True, host='0.0.0.0', port=5556)
